import { Request, Response } from 'express';
import { BaseRepository } from '../repository.service/repository.service';
import { validateNewspost } from '../validation/valid';
import { ValidationError, NewsRepositoryError } from '../errors/Errors'

export class BaseController {
    private table: string;
    private repository: BaseRepository;

    constructor(table: string, repository: BaseRepository) {
        this.table = table;
        this.repository = repository;

    
        this.addNews = this.addNews.bind(this);
        this.getItems = this.getItems.bind(this);
        this.getOneItems = this.getOneItems.bind(this);
        this.updateNews = this.updateNews.bind(this);
        this.deleteNews = this.deleteNews.bind(this);
    }

    async getItems (req: Request, res: Response) {
        try {
            const data = await this.repository.getItems(this.table, req, res)
            res.status(200).json({ data });

            if (data === undefined) {
                res.status(200).json([]); 
            }
        } catch (error) {
            console.error("Error find news:", error);
            res.status(500).json({ error: "Error find news" }); 
        }
    }
    
    async getOneItems (req: Request, res: Response) {
        try {
            const data = await this.repository.getOneItems(this.table, req, res);
            if (data) {
                res.status(200).json({ data, message: `${this.table} read` });
            } else {
                res.status(404).json({ errorMessage: "News not found" });
            }
        } catch (error) {
            console.error("Error find news:", error);
            res.status(500).json({ error: "Error find news" });
        }
    }
    
    async addNews (req: Request, res: Response) {
        try {
            const validate =  validateNewspost(req.body);
            if (validate !== true) {
                throw new ValidationError('Validation error', validate);
            }
            const data = await this.repository.addNews(this.table, req, res);

            res.status(201).json({ message: `${this.table} created`, data });
        } catch (error) {
            if (error instanceof ValidationError) {
                console.error("Validation error:", error);
                res.status(400).json({ error: error.message, details: error.details });
            } else {
                console.error("Error adding news:", error);
                res.status(500).json({ error: "Error adding news" });
            }
        }
    }
    
    async updateNews (req: Request, res: Response) {
        // console.log(req.body)
        try {
            const validate =  validateNewspost(req.body);
            console.log(validate)
            if (validate !== true) {
                throw new ValidationError('Validation error', validate);
            }

            const data = await this.repository.updateNews(this.table, req, res);
            console.log(data)
            if (data.errorMessage) {
                res.status(404).json(data.errorMessage);
            }
            res.status(200).json({ data, message: `${this.table} updated` });

        } catch (error) {
            if (error instanceof ValidationError) {
                console.error("Validation error:", error);
                res.status(400).json({ error: error.message, details: error.details });
            } else {
                console.error("Error updating news:", error);
                res.status(500).json({ error: "Error updating news" });
            }
        }
    }
    
    async deleteNews (req: Request, res: Response) {
        try {
            const data = await this.repository.deleteNews(this.table, req, res);
            if (data.errorMessage) {
                res.status(404).json(data.errorMessage);
            }
            res.status(200).json(data);
        } catch (error) {
          console.error("Error deleting news:", error);
          res.status(500).json({ error: "Error deleting news" });
        }
    }

    async getError(req: Request, res: Response) {
        try {
            throw new NewsRepositoryError('Error in NewsRepository');
        } catch (error) {
            if (error instanceof NewsRepositoryError) {
                console.error("NewsRepository error:", error);
                res.status(500).json({ error: error.message });
            } else {
                console.error("Unknown error:", error);
                res.status(500).json({ error: "Unknown error" });
            } 
        }
    }

}