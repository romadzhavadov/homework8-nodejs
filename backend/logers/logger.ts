import winston from 'winston';
import { Request, Response, NextFunction } from 'express';
import path from 'path';
import { ValidationError } from '../errors/Errors';

const logsDir = path.join(__dirname, 'logs');
console.log(logsDir)

export const logger = winston.createLogger({
  level: 'info',
  format: winston.format.combine(
          winston.format.timestamp(),
          winston.format.json()
  ),

  transports: [
    new winston.transports.Console(),
    new winston.transports.File({ filename: path.join(logsDir, 'app.log') })
  ]
});


export function logRequests(req: Request, res: Response, next: NextFunction) {
  logger.info(`${req.method} ${req.url}`, { body: req.body });
  next();
}

export function errorLogger(err: Error, req: Request, res: Response, next: NextFunction) {
  if (err instanceof ValidationError) {
    logger.warn(err.message);
  } else {
    logger.error(`${err.message}\n${err.stack}`);
  }
  next(err);
}


// err.name === 'ValidationError'