export default {
    name: 'newsposts',
    fields: [
        {
            name: 'id',
            type: 'increments',
            positive: true,
            integer: true
        },
        {
            name: 'title',
            type: 'string',
            max: 50
        },
        {
            name: 'text',
            type: 'string',
            max: 256
        },
        {
            name: 'genre',
            type: 'string',
            enum: ['Politic', 'Business', 'Sport', 'Other']
        },
        {
            name: 'isPrivate',
            type: 'boolean'
        },
        {
            name: 'createDate',
            type: 'timestamp',
        }
    ]
}

