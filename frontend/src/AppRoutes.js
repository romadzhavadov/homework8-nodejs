import React from 'react';
import { Route, Routes } from 'react-router-dom';
import NewsPage from './pages/NewsPage/NewsPage';
import AddEditNewsPage from './pages/AddEditNewsPage/AddEditNewsPage';
import NewsDetailsPage from './pages/NewsDetailsPage/NewsDetailsPage';


function AppRoutes() {


  return (
    <Routes>
      <Route path='/' element={<NewsPage />} />
      <Route path='/edit/:id' element={<AddEditNewsPage />} />
      <Route path='/:id' element={<NewsDetailsPage />} />
      <Route path='/edit' element={<AddEditNewsPage />} />
      <Route path="*" element={<h1>404 - PAGE NO FOUND</h1>} />
    </Routes>

  )
}

export default AppRoutes;